/**
 * Universidad del Valle de Guatemala
 * Jose Javier Jo Escobar,14343
 * Algoritmo de thomson
 * * Se utilizaron las siguientes herramientas y referencias para probar expresiones
 * regulares en  el método de replaceAll
 * https://regex101.com/r/vW2pQ7/30
 * http://stackoverflow.com/questions/632475/regex-to-pick-commas-outside-of-quotes
 */

import java.util.HashMap;
import java.io.File;


public class Main {
    public static String EPSILON = "ε";
    public static char EPSILON_CHAR = EPSILON.charAt(0);
    public static ErrorSintactico errores = new ErrorSintactico();


    public static void main(String[] args) {
                    LeerArchivo leer = new LeerArchivo();
                    File file = new File("cocol"+".txt");
                    HashMap cocol = leer.leerArchivo(file);
                    System.out.println("Procesando...");
                    System.out.println("esto tardara unos segundos...");
                    Lexer lexer = new Lexer(cocol);
                    lexer.vocabulario();
                    lexer.construct(cocol);
                    System.out.println("Archivo de Cocol Aceptado.");
                    System.out.println("El Numero de errores encontrados es: "+errores.getCount());

                    if(errores.getCount()==0){
                        //System.out.println(lexer.getSetDeclaration().size());
                        Generador generador = new Generador(lexer);
                        generador.geneDatos();
                        Plantilla plantilla = new Plantilla();
                        File datos = new File("datos"+".txt");
                        plantilla.LectorDeArchivo(datos);
                        generador.geneLexer();


                    }

        }
}






