public class ErrorSintactico {
    private int count = 0;

    public ErrorSintactico(){ }

    public void errorSintactico(int line,  String msg){
        System.out.println("");
        System.out.println("Error Sintáctico " + "en la línea " +line);
        System.out.println(">>>   " + msg);
        System.out.println("");
        count++;

    }

    public void errorSemantico(int line, String msg){
        System.out.println("");
        System.out.println("Error semántico " + "en la línea " +line);
        System.out.println(">>>   " + msg);
        System.out.println("");
        count++;


    }

    public void Warning(int line, String msg){
        System.out.println("");
        System.out.println("Advertencia " + "en la línea " +line);
        System.out.println(">>>   " + msg );
        System.out.println("");
        count++;

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
