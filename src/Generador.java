import java.io.IOException;
import java.io.PrintWriter;

public class Generador {


    Lexer lexer;


    public Generador(Lexer lexer) {
        this.lexer = lexer;
    }

    /**
     * Generamos el archivo que contendra los set declaration
     * keywords y tokens en arraylist
     */

    public void geneDatos() {
        try {
            PrintWriter writer = new PrintWriter("datos.txt", "UTF-8");
            String setDeclaration= " ";
            for (String set : lexer.getSetDeclaration()){
                setDeclaration += set+" ☺ ";
            }
            writer.println(setDeclaration);

            String keywords = " ";
            for (String set : lexer.getKeywords()){
                keywords += set+" ☺ ";
            }
            writer.println(keywords);

            String tokens = " ";
            for (String set : lexer.getTokens()){
                tokens += set+" ☺ ";
            }
            writer.println(tokens);
            writer.close();
        } catch (IOException ex) {

        }
    }

    public void geneLexer(){
        try{
            PrintWriter writer = new PrintWriter("ArchivoLexer.java", "UTF-8");
            writer.println ("import java.io.BufferedReader;\n" +
                    "import java.io.File;\n" +
                    "import java.io.FileReader;\n" +
                    "import java.util.ArrayList;\n" +
                    "import java.util.HashMap;\n" +
                    "\n" +
                    "public class ArchivoLexer {\n" +
                    "\n" +
                    "    public static ArrayList<String> KEYWORDS = new ArrayList<>();\n" +
                    "    public static ArrayList<String> TOKENS = new ArrayList<>();\n" +
                    "    public static ArrayList<String> SETDECLARATION = new ArrayList<>();\n" +
                    "    public static ArrayList<String> GramarContent = new ArrayList<>();\n" +
                    "    public static ArrayList<String> Input = new ArrayList<>();\n" +
                    "    private final HashMap<String,Boolean> Keywords;\n" +
                    "\n" +
                    "    public ArchivoLexer(HashMap cadena) {\n" +
                    "        this.Keywords = new HashMap<>();\n" +
                    "    }\n" +
                    "\n" +
                    "    public static void main(String[] args) {\n" +
                    "\n" +
                    "        System.out.println(\"Bienvenido al Lexer\");\n" +
                    "        LectorDeArchivo(new File(\"C:\\\\Users\\\\Jose Jo Escobar\\\\IdeaProjects\\\\thomson\\\\proyecto-2-compiladores\\\\datos.txt\"));\n" +
                    "        leerInput(\"C:\\\\Users\\\\Jose Jo Escobar\\\\IdeaProjects\\\\thomson\\\\proyecto-2-compiladores\\\\input.txt\");\n" +
                    "\n" +
                    "\n" +
                    "    }\n" +
                    "\n" +
                    "    public static void leerInput(String direccion){\n" +
                    "\n" +
                    "        try{\n" +
                    "            BufferedReader bf = new BufferedReader(new FileReader(direccion));\n" +
                    "            String linea = \"\";\n" +
                    "            while(( linea = bf.readLine()) != null){\n" +
                    "                Input.add(linea);\n" +
                    "            }\n" +
                    "\n" +
                    "        }catch (Exception e){\n" +
                    "            System.err.println(\"No se encontro le archivo\");\n" +
                    "\n" +
                    "        }\n" +
                    "\n" +
                    "    }\n" +
                    "\n" +
                    "\n" +
                    "        public static void LectorDeArchivo(File archivo){\n" +
                    "        try{\n" +
                    "            BufferedReader bf = new BufferedReader(new FileReader(archivo));\n" +
                    "            String Linea=\"\";\n" +
                    "            while((Linea = bf.readLine()) !=null){\n" +
                    "\n" +
                    "                GramarContent.add(Linea);\n" +
                    "\n" +
                    "            }\n" +
                    "            /**\n" +
                    "             * Agregamos el contenido del archivo datos.txt a un arraylist separado\n" +
                    "             */\n" +
                    "\n" +
                    "            String[] SETDeclaration = GramarContent.get(0).split(\"☺\");\n" +
                    "            for(String i : SETDeclaration){\n" +
                    "\n" +
                    "                SETDECLARATION.add(i);\n" +
                    "            }\n" +
                    "\n" +
                    "\n" +
                    "            String[] Keywords = GramarContent.get(1).split(\"☺\");\n" +
                    "            for(String i : Keywords){\n" +
                    "\n" +
                    "\n" +
                    "                KEYWORDS.add(i);\n" +
                    "            }\n" +
                    "\n" +
                    "\n" +
                    "            String[] Tokens = GramarContent.get(2).split(\"☺\");\n" +
                    "            for(String i : Tokens){\n" +
                    "                TOKENS.add(i);\n" +
                    "            }\n" +
                    "\n" +
                    "        }catch(Exception e){\n" +
                    "\n" +
                    "        }\n" +
                    "    }\n" +
                    "    /*\n" +
                    "    public void Conversor(){\n" +
                    "            \n" +
                    "            Regex regex = new Regex();\n" +
                    "            Keywords.put(\"ANY\", regex.ab.abreviacionOr(ANY));\n" +
                    "            \n" +
                    "            for (Map.Entry<Integer,String> entry : cadena.entrySet()){\n" +
                    "                String value = entry.getValue();\n" +
                    "                if (value.contains(\"CHARACTERS\")){\n" +
                    "                    int linea = entry.getKey();\n" +
                    "                    while (true){\n" +
                    "                        linea = avanzar(linea);\n" +
                    "                        if(this.cadena.get(linea).contains(\"KEYWORDS\"))\n" +
                    "                            break;\n" +
                    "                        String valor = this.cadena.get(linea);\n" +
                    "                        \n" +
                    "                        valor = valor.trim();\n" +
                    "                        int index = valor.indexOf(\"=\");\n" +
                    "                        String ident = valor.substring(0,index);\n" +
                    "                        String revisar = valor.substring(++index,valor.length()-1);\n" +
                    "                        revisar = revisar.trim();\n" +
                    "                        revisar = crearCadenasConOR(revisar);\n" +
                    "                        \n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }\n" +
                    "            \n" +
                    "            \n" +
                    "    }*/\n" +
                    "\n" +
                    "\n" +
                    "}\n");
            writer.close ();

        }catch(IOException ex){

        }
    }


}
