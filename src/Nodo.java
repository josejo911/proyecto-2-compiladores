/**
 * Universidad del Valle de Guatemala
 * Jose Javier Jo Escobar,14343
 * Clase encargada de la administracion de nodos en el arbol
 */

public class Nodo<T> implements Comparable<Nodo> {

    private T id;
    private T regex;
    private int numNodo;
    private Nodo izq, der;
    private boolean isLeaf;


    public Nodo(T regex) {
        this.regex = regex;
        this.izq = new Nodo();
        this.der = new Nodo();
    }

        public Nodo() {    }


        public int getNumeroNodo() { return numNodo; }


    @Override
    public int compareTo(Nodo o) {
        return Integer.compare(numNodo, o.getNumeroNodo());
    }


    @Override
    public String toString() {
        String regexd = "" + numNodo;
        return regexd;
    }
}


