/**
 * Universidad del Valle de Guatemala
 * Jose Javier Jo Escobar,14343
 * Algoritmo de thomson
 * Clase encargada de cad auna de las transiciones en el automata
 */
public class Transiciones<T> {

    private Estados inicio;
    private Estados fin;
    private T token;

    public Transiciones(Estados inicio, Estados fin, T simbolo) {
        this.inicio = inicio;
        this.fin = fin;
        this.token = simbolo;
    }


    public Estados getFin() {
        return fin;
    }


    public T getSimbolo() {
        return token;
    }


    @Override
    public String toString(){
        return "(" + inicio.getId() +"-" + token  +"-"+fin.getId()+")";
    }

}
